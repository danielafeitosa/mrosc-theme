(function($) {
    var $replaceOnHover = {
      'before': 'after',
      'Comunicação': 'colaborativa',
      'Experiência': 'colaborativa',
      'Relatoria': 'compartilhada',
      'Convidados': 'confirmados',
      'Arena': 'participação social',
      'Como Chegar': 'mais informações'
    }

    var $specialCases = {
      'case': 'change',
      'participação social': 'letter-spacing',
      'mais informações': 'letter-spacing',
      'Rede de pesquisa': 'letter-spacing',
      'Rede de Pesquisa': 'letter-spacing'
    }

    var before;
    var after;

    $('.link-list-block a').hover (
      function() {
        var element = $(this);
        before = element.html();
        after = $replaceOnHover[before];
        var changeSpecial = $specialCases[after];

        if (after) {
          element.html(after);
        }
        if (changeSpecial) {
          element.css(changeSpecial, '1px');
        }
      },
      function() {
        var element = $(this);

        if (after) {
          element.html(before).css('letter-spacing', '5px');
        }
      }
    );

  $('.link-list-block a').each(function() {
    var element = $(this);
    if (element.html().length > 13) {
      element.css('letter-spacing', '1px');
    }
  });

  jQuery('.link-list-block').first().wrap("<div class='colored-links'></div>");

  jQuery(".box-2 .link-list-block .block-title").click(function(){
    jQuery(this).parent().children("ul").stop().slideToggle();
  });

  $('#dates .date').click (
    function() {
      var element = $(this);
      $('.date').removeClass('date-active');
      element.addClass('date-active');
      var dateId = element.attr('id');
      $('.lectures').hide();
      $('#lectures').find('.' + dateId).show();
    });

})(jQuery);
